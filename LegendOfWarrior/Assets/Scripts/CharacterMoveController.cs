﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    CharacterController m_controller;

    public float speed = 10.0f;
    public float gravity = 9.8f;
    public float rotSpeed = 10.0f;
    private Vector3 moveDirection = Vector3.zero;

    void Start()
    {
        m_controller = GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
       
        if (m_controller.isGrounded)
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            

            moveDirection = new Vector3(x, 0, z) * speed;
        }
        moveDirection.y -= gravity * Time.deltaTime;
        m_controller.Move(moveDirection * Time.deltaTime);
        float MouseX = Input.GetAxis("Mouse X");
        transform.Rotate(Vector3.up * rotSpeed * MouseX);
    }
}

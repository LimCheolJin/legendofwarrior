﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessCollisionMonster : MonoBehaviour
{
    // Start is called before the first frame update

    private float AttackTimer= 0f;
    private float AttackRate = 1f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Monster")
        {
            if (AttackTimer > AttackRate)
            {
                gameObject.GetComponent<PlayerHpProcess>().playerHP -= 10;
                Debug.Log(gameObject.GetComponent<PlayerHpProcess>().playerHP);
                AttackTimer = 0f;
            }
            else
            {
                AttackTimer += Time.deltaTime;
            }
        }
    }
}

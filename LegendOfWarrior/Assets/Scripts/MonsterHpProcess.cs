﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterHpProcess : MonoBehaviour
{
    // Start is called before the first frame update

    public int monsterHP ;
    private GameObject Score;

    void Start()
    {
        Score = GameObject.FindWithTag("ScoreCount");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckMonsterHP();
    }

    void CheckMonsterHP()
    {
        if(monsterHP <= 0)
        {
            Score.GetComponent<Score>().score += 300;
            gameObject.SetActive(false);
        }
    }

    public void SetHP(int hp)
    {
        monsterHP = hp;
    }
}

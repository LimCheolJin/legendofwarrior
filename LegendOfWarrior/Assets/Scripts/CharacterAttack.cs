﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAttack : MonoBehaviour
{
    // Start is called before the first frame update

    public float attackRange = 10.0f;
    public Transform startPoint;

    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        startPoint = gameObject.transform;
        
    }


    public void CharacterAttackLogic()
    {
        Debug.Log("Attack Logic");
        Debug.DrawRay(startPoint.position, startPoint.forward * attackRange, Color.green);
    }
}

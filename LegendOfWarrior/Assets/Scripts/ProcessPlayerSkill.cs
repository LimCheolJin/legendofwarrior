﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProcessPlayerSkill : MonoBehaviour
{
    // Start is called before the first frame update

    private GameObject Player;
    public Text text;
    public GameObject playerObject;
    public GameObject particle;
    public GameObject timerObject;
    public int skillCount = 0;
    private int updateCount = 0;
    bool isableSkill = false;
    int particlecount = 0;
    bool particleFlag = false;
    private GameObject p;

    void Start()
    {
        Player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       
        updateCount++;
        if (updateCount >= 50)
        {
           
            CheckUseSkill();
            updateCount = 0;

            if (particleFlag == true)
            {
                particlecount++;
            }
        }

        if (particlecount > 3)
        {
            particleFlag = false;
            particlecount = 0;
            p.SetActive(false);
        }

        text.text = skillCount.ToString();
        
    }

    void CheckUseSkill()
    {
        float t = timerObject.GetComponent<Timer>().time;
        int integerTime = (int)t;

        Debug.Log("Time " + integerTime);
       
        if ((integerTime % 30 == 0) && (integerTime != 0))
        {
            skillCount++;
            Debug.Log("skillcount: " + skillCount);
        }

        if (skillCount > 0)
        {
            
            if (Input.GetKey(KeyCode.E))
            {
                particleFlag = true;
                p = Instantiate(particle, Player.transform.position, Player.transform.rotation);
                playerObject.GetComponent<ChangeAnimation>().isUseSkill = true;
                isableSkill = true;
                skillCount--;
                Debug.Log(skillCount);
            }
            else
            {
                isableSkill = false;
                playerObject.GetComponent<ChangeAnimation>().isUseSkill = false;
            }

          
        }
        else
        {
            skillCount = 0;
            isableSkill = false;
            playerObject.GetComponent<ChangeAnimation>().isUseSkill = false;
           
        }

    }


    void OnTriggerStay(Collider other)
    {

        if (isableSkill == true)
        {
            if (other.tag == "Monster")
            {
                other.GetComponent<MonsterHpProcess>().monsterHP -= 100;
                Debug.Log("skill:  " + other.name);       
                Debug.Log("skillcount: " + skillCount);
                
            }
        }


    }
}

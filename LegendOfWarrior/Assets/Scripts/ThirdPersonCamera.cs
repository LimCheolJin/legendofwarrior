﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform target;
    public float dist = 7.0f;
    public float height = 5.0f;

    private Transform cameraTransform;
    void Start()
    {
        cameraTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        cameraTransform.position = target.position - (1 * Vector3.forward * dist) + (Vector3.up * height);
        cameraTransform.LookAt(target);
    }
}

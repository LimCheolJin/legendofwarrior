﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessCollisionSword : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject playerObject;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
     
        if (playerObject.GetComponent<ChangeAnimation>().isAttack)
        {
            if (other.tag == "Monster")
            {
                other.GetComponent<MonsterHpProcess>().monsterHP -= 50;
                Debug.Log(other.GetComponent<MonsterHpProcess>().monsterHP);
                playerObject.GetComponent<ChangeAnimation>().isAttack = false;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterAI : MonoBehaviour
{

    private NavMeshAgent Navagent;
    private GameObject Player;

    public GameObject Monster;
    private Animator MonsterAnim;

    private bool collision = false;

    private void Awake()
    {
        Navagent = GetComponent<NavMeshAgent>();
        Player = GameObject.FindWithTag("Player");
        MonsterAnim = Monster.GetComponent<Animator>();
        MonsterAnim.SetBool("IsRun", true);
    }

 

    private void Update()
    {        
        Navagent.SetDestination(Player.transform.position);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            MonsterAnim.SetBool("IsAttack", true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            MonsterAnim.SetBool("IsAttack", false);
        }
    }
}

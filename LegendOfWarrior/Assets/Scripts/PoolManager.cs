﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PoolManager : MonoBehaviour
{

    public GameObject SlimePrefab;
    public GameObject TurtlePrefab;
    public GameObject BossPrefab;

    public List<GameObject> Slimeprefabs;
    public List<GameObject> Turtleprefabs;
    public List<GameObject> Bossprefabs;

    void SpawnSlime()
    {
        Spawn("SlimePrefab");
    }

    void SpawnTurtle()
    {
        Spawn("TurtlePrefab");
    }

    void SpawnBoss()
    {
        Spawn("BossPrefab");
    }

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnSlime", 1, 1);
        InvokeRepeating("SpawnTurtle", 1, 2);
        InvokeRepeating("SpawnBoss", 3, 5);
    }

    private void Awake()
    {
        Slimeprefabs = new List<GameObject>();
        Turtleprefabs = new List<GameObject>();
        Bossprefabs = new List<GameObject>();
    }

    public void Spawn(string prefabname)
    {
        GameObject founded;

        float randomX = Random.Range(-27f, 27f);
        float randomZ = Random.Range(-27f, 27f);

        if (prefabname == "SlimePrefab")
        {
            founded = Slimeprefabs.FirstOrDefault(go => !go.activeInHierarchy);

            if (founded == null)
            {
                founded = (GameObject)Instantiate(SlimePrefab, new Vector3(randomX, 2f, randomZ), Quaternion.identity);
                Slimeprefabs.Add(founded);
            }
            else
            {
                founded.SetActive(true);
                founded.GetComponent<MonsterHpProcess>().SetHP(100);
                founded.transform.position = new Vector3(randomX, 2f, randomZ);
            }

        }
        else if (prefabname == "TurtlePrefab")
        {
            founded = Turtleprefabs.FirstOrDefault(go => !go.activeInHierarchy);
            if (founded == null)
            {
                founded = (GameObject)Instantiate(TurtlePrefab, new Vector3(randomX, 2f, randomZ), Quaternion.identity);
                Turtleprefabs.Add(founded);
            }
            else
            {
                founded.SetActive(true);
                founded.GetComponent<MonsterHpProcess>().SetHP(100);
                founded.transform.position = new Vector3(randomX, 2f, randomZ);
            }
        }
        else if (prefabname == "BossPrefab")
        {
            founded = Bossprefabs.FirstOrDefault(go => !go.activeInHierarchy);
            if (founded == null)
            {
                founded = (GameObject)Instantiate(BossPrefab, new Vector3(randomX, 2f, randomZ), Quaternion.identity);
                Bossprefabs.Add(founded);
            }
            else
            {
                founded.SetActive(true);
                founded.GetComponent<MonsterHpProcess>().SetHP(300);
                founded.transform.position = new Vector3(randomX, 2f, randomZ);
            }
        }


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeAnimation : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject character;
    private Animator characterAnim;
    private bool isMove;
    public bool isAttack;
    public bool isUseSkill = false;
  


    void Start()
    {
        characterAnim = character.GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckRunable();
        CheckAttackable();
    }

    void CheckRunable()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            isMove = true;
            
        }
        else 
        {
            isMove = false;

        }


        if (isMove)
        {
           
            characterAnim.SetBool("Run", true);
        }
        else
        {
            
            characterAnim.SetBool("Run", false);
        }
    }
    void CheckAttackable()
    {

        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Attack start");
            characterAnim.Play("TH Sword Melee Attack 02");
            isAttack = true;
        }
        //else
        //{
        //    Debug.Log("Attack Disable");
        //    //characterAnim.Play("Idle");
        //    //characterAnim.StopPlayback();
        //}

    }

    public void AttackEndProcess()
    {
        isAttack = false;
        Debug.Log("Attack end");
    }
}
